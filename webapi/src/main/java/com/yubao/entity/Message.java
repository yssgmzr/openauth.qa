package com.yubao.entity;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author yubaolee
 * @since 2020-04-17
 */
@ApiModel(value="Message对象", description="")
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private Date time;

    private String content;

    private String href;

    @TableField("`from`")
    private String from;

    @TableField("`to`")
    private String to;

    @ApiModelProperty(value = "来源方状态")
    private Integer fromstatus;

    @ApiModelProperty(value = "到达方状态")
    private Integer tostatus;

    @ApiModelProperty(value = "类型")
    private String type;

    @ApiModelProperty(value = "来源方名称")
    private String fromname;

    @ApiModelProperty(value = "到达方名字")
    private String toname;

    @TableField("`read`")
    private Integer read;

    private String title;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }


    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }


    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }


    public Integer getFromstatus() {
        return fromstatus;
    }

    public void setFromstatus(Integer fromstatus) {
        this.fromstatus = fromstatus;
    }


    public Integer getTostatus() {
        return tostatus;
    }

    public void setTostatus(Integer tostatus) {
        this.tostatus = tostatus;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getFromname() {
        return fromname;
    }

    public void setFromname(String fromname) {
        this.fromname = fromname;
    }


    public String getToname() {
        return toname;
    }

    public void setToname(String toname) {
        this.toname = toname;
    }


    public Integer getRead() {
        return read;
    }

    public void setRead(Integer read) {
        this.read = read;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
