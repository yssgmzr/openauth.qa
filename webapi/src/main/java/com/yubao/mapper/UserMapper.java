package com.yubao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubao.entity.User;
import com.yubao.entity.UserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper extends BaseMapper<User> {
    int countByExample(UserExample example);

    List<User> selectByExample(UserExample example);


}