package com.yubao.test;

import com.alibaba.fastjson.JSON;
import com.yubao.Appllication;
import com.yubao.entity.Message;
import com.yubao.request.MsgListReq;
import com.yubao.response.AnswerViewModel;
import com.yubao.response.PageObject;
import com.yubao.response.UserViewModel;
import com.yubao.service.LoginService;
import com.yubao.service.MessageService;
import com.yubao.util.NormalException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Appllication.class)
public class TestMsg {

    @MockBean
    LoginService userInfoService;

    @Autowired
    MessageService messageService;

    private static final Logger LOGGER = LoggerFactory.getLogger(TestMsg.class);

    @Before
    public void setUp() throws NormalException {
        UserViewModel mockuser = new UserViewModel();
        mockuser.setName("admin");
        mockuser.setId("7e7c70a1-f882-43b9-8504-743a90a4d294");
        mockuser.setRmb(2);
        when(userInfoService.get()).thenReturn(mockuser);

    }


    @Test
    public void queryMsgList() throws Exception {
        MsgListReq req = new MsgListReq();
        req.setIndex(1);
        req.setSize(10);
        PageObject<Message> result = messageService.queryMsgList(req);
        LOGGER.info(JSON.toJSONString(result));
    }




}
